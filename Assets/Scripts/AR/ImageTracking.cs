using NRKernal;
using System.Collections.Generic;
using UnityEngine;

public class ImageTracking : MonoBehaviour
{
    public List<GameObject> goList = new List<GameObject>();
    public List<GameObject> objs;
    private OperateAndDebug opDebug;
    private List<NRTrackableImage> m_TempTrackingImages = new List<NRTrackableImage>();
    private Vector3 tempPos = Vector3.zero;
    private Vector3 tempRot = Vector3.zero;
    private Vector3 tempSca = Vector3.zero;
    private GameObject curtObj = null;

    void Start()
    {
        opDebug = GetComponent<OperateAndDebug>();
        if (opDebug.isExists)
        {
            goList[0].transform.localScale = goList[0].transform.localScale + opDebug.scaList[0];
            goList[1].transform.localScale = goList[1].transform.localScale + opDebug.scaList[1];
            goList[2].transform.localScale = goList[2].transform.localScale + opDebug.scaList[2];
            goList[3].transform.localScale = goList[3].transform.localScale + opDebug.scaList[3];
            goList[4].transform.localScale = goList[4].transform.localScale + opDebug.scaList[4];
            goList[5].transform.localScale = goList[5].transform.localScale + opDebug.scaList[5];
            goList[6].transform.localScale = goList[6].transform.localScale + opDebug.scaList[6];
        }
    }

    public void Update()
    {
#if !UNITY_EDITOR
            // Check that motion tracking is tracking.
            if (NRFrame.SessionStatus != SessionState.Running)
            {
                return;
            }
#endif
        NRFrame.GetTrackables<NRTrackableImage>(m_TempTrackingImages, NRTrackableQueryFilter.All);
        foreach (var image in m_TempTrackingImages)
        {
            if (image.GetTrackingState() == TrackingState.Tracking)
            {
                //tyre.transform.position = image.GetCenterPose().position;
                //tyre.transform.rotation = image.GetCenterPose().rotation;
                //tyre.transform.eulerAngles = new Vector3(tyre.transform.eulerAngles.x + 90f, 
                //tyre.transform.eulerAngles.y, tyre.transform.eulerAngles.z);
                Debug.LogError("ͼƬ��Index="+image.GetDataBaseIndex());
                if (objs.Count > 0)
                {   
                    foreach (GameObject go in objs)
                    {
                        go.SetActive(false);
                    }
                }
                switch (image.GetDataBaseIndex())
                {
                    case 0:
                        if (opDebug.isExists)
                        {
                            goList[0].transform.localPosition = image.GetCenterPose().position + opDebug.posList[0];
                            goList[0].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles + opDebug.rotList[0];
                        }
                        else
                        {
                            goList[0].transform.localPosition = image.GetCenterPose().position;
                            goList[0].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles;
                        }
                        ShowObj(0);
                        curtObj = goList[0];
                        break;
                    case 1:
                        if (opDebug.isExists)
                        {
                            goList[1].transform.localPosition = image.GetCenterPose().position + opDebug.posList[1];
                            goList[1].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles + opDebug.rotList[1];
                        }
                        else
                        {
                            goList[1].transform.localPosition = image.GetCenterPose().position;
                            goList[1].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles;
                        }
                        ShowObj(1);
                        curtObj = goList[1];
                        break;
                    case 2:
                        if (opDebug.isExists)
                        {
                            goList[2].transform.localPosition = image.GetCenterPose().position + opDebug.posList[2];
                            goList[2].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles + opDebug.rotList[2];
                        }
                        else
                        {
                            goList[2].transform.localPosition = image.GetCenterPose().position;
                            goList[2].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles;
                        }
                        ShowObj(2);
                        curtObj = goList[2];
                        break;
                    case 3:
                        if (opDebug.isExists)
                        {
                            goList[3].transform.localPosition = image.GetCenterPose().position + opDebug.posList[3];
                            goList[3].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles + opDebug.rotList[3];
                        }
                        else
                        {
                            goList[3].transform.localPosition = image.GetCenterPose().position;
                            goList[3].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles;
                        }
                        ShowObj(3);
                        curtObj = goList[3];
                        break;
                    case 4:
                        if (opDebug.isExists)
                        {
                            goList[4].transform.localPosition = image.GetCenterPose().position + opDebug.posList[4];
                            goList[4].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles + opDebug.rotList[4];
                        }
                        else
                        {
                            goList[4].transform.localPosition = image.GetCenterPose().position;
                            goList[4].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles;
                        }
                        ShowObj(4);
                        curtObj = goList[4];
                        break;
                    case 5:
                        if (opDebug.isExists)
                        {
                            goList[5].transform.localPosition = image.GetCenterPose().position + opDebug.posList[5];
                            //goList[5].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles + opDebug.rotList[5];
                        }
                        else
                        {
                            goList[5].transform.localPosition = image.GetCenterPose().position;
                        }
                        ShowObj(5);
                        curtObj = goList[5];
                        break;
                    case 6:
                        if (opDebug.isExists)
                        {
                            goList[6].transform.localPosition = image.GetCenterPose().position + opDebug.posList[6];
                            goList[6].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles + opDebug.rotList[6];
                        }
                        else
                        {
                            goList[6].transform.localPosition = image.GetCenterPose().position;
                            goList[6].transform.localEulerAngles = image.GetCenterPose().rotation.eulerAngles;
                        }
                        ShowObj(6);
                        curtObj = goList[6];
                        break;
                    default:
                        break;
                }

                tempPos = curtObj.transform.localPosition;
                tempRot = curtObj.transform.rotation.eulerAngles;
                tempSca = curtObj.transform.localScale;
            }
            else
            {
                if (curtObj != null)
                {
                    curtObj.transform.position = tempPos;
                    curtObj.transform.eulerAngles = tempRot;
                    curtObj.transform.localScale = tempSca;
                }
            }
        }
    }

    private void ShowObj(int index)
    {
        for (int i = 0; i < goList.Count; i++)
        {
            if (i == index)
            {
                if (!goList[i].activeSelf)
                {
                    goList[i].SetActive(true);
                }
            }
            else
            {
                if (goList[i].activeSelf)
                {
                    goList[i].SetActive(false);
                }
            }
        }
    }

    public void EnableImageTracking()
    {
        Debug.LogError("����ͼƬʶ��");
        var config = NRSessionManager.Instance.NRSessionBehaviour.SessionConfig;
        config.ImageTrackingMode = TrackableImageFindingMode.ENABLE;
        NRSessionManager.Instance.SetConfiguration(config);
    }

    public void DisableImageTracking()
    {
        var config = NRSessionManager.Instance.NRSessionBehaviour.SessionConfig;
        config.ImageTrackingMode = TrackableImageFindingMode.DISABLE;
        NRSessionManager.Instance.SetConfiguration(config);
    }
}