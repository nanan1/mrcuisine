using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scale : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // public static float fScaleOriginal = transform.localScale.x;
        // public static float fScaleInterval = 0.00001f;
        float fScale = 0.0f;
        transform.localScale = new Vector3(fScale, fScale, fScale);
    }

    // Update is called once per frame
    void Update()
    {
        float fScale = transform.localScale.x;
        float fScaleInterval = 0.001f;
        float fScaleMax = 1.0f;
        if (fScale < fScaleMax)
        {
            fScale += fScaleInterval;
        }
        transform.localScale = new Vector3(fScale, fScale, fScale);
    }
}
