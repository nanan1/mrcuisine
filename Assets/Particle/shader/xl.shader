// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33602,y:32311,varname:node_3138,prsc:2|custl-6908-RGB;n:type:ShaderForge.SFN_TexCoord,id:4066,x:32525,y:32134,varname:node_4066,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Append,id:362,x:32962,y:32147,varname:node_362,prsc:2|A-4066-U,B-205-OUT;n:type:ShaderForge.SFN_RemapRange,id:205,x:32702,y:32302,varname:node_205,prsc:2,frmn:0,frmx:1,tomn:1,tomx:0|IN-4066-V;n:type:ShaderForge.SFN_Tex2d,id:6908,x:33257,y:32601,ptovrint:False,ptlb:node_6908,ptin:_node_6908,varname:_node_6908,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-8447-UVOUT;n:type:ShaderForge.SFN_UVTile,id:8447,x:33092,y:32601,varname:node_8447,prsc:2|UVIN-362-OUT,WDT-9197-OUT,HGT-5969-OUT,TILE-2585-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9197,x:32750,y:32485,ptovrint:False,ptlb:U,ptin:_U,varname:_U,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Negate,id:5969,x:32806,y:32664,varname:node_5969,prsc:2|IN-1054-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1054,x:32537,y:32640,ptovrint:False,ptlb:V,ptin:_V,varname:_V,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Time,id:3912,x:32546,y:32908,varname:node_3912,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:8882,x:32591,y:33118,ptovrint:False,ptlb:Speed,ptin:_Speed,varname:_Speed,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:4309,x:32787,y:32963,varname:node_4309,prsc:2|A-3912-T,B-8882-OUT;n:type:ShaderForge.SFN_Trunc,id:2585,x:33009,y:32931,varname:node_2585,prsc:2|IN-4309-OUT;proporder:6908-9197-1054-8882;pass:END;sub:END;*/

Shader "qmy/xl" {
    Properties {
        _node_6908 ("node_6908", 2D) = "white" {}
        _U ("U", Float ) = 0
        _V ("V", Float ) = 0
        _Speed ("Speed", Float ) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _node_6908; uniform float4 _node_6908_ST;
            uniform float _U;
            uniform float _V;
            uniform float _Speed;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
                float4 node_3912 = _Time;
                float node_2585 = trunc((node_3912.g*_Speed));
                float2 node_8447_tc_rcp = float2(1.0,1.0)/float2( _U, (-1*_V) );
                float node_8447_ty = floor(node_2585 * node_8447_tc_rcp.x);
                float node_8447_tx = node_2585 - _U * node_8447_ty;
                float2 node_8447 = (float2(i.uv0.r,(i.uv0.g*-1.0+1.0)) + float2(node_8447_tx, node_8447_ty)) * node_8447_tc_rcp;
                float4 _node_6908_var = tex2D(_node_6908,TRANSFORM_TEX(node_8447, _node_6908));
                float3 finalColor = _node_6908_var.rgb;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
