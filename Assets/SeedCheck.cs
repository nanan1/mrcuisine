using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedCheck : MonoBehaviour
{
    bool enter;
    int i = 0;
    public GameObject WholeTerrain;
    public GameObject[] Terrains;
    public GameObject[] Seeds;
    public GameObject[] Flowers;
    public GameObject LightTrig;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            WholeTerrain.SetActive(false);
            for (int i = 0; i < Terrains.Length; i++)
            {
                Terrains[i].SetActive(false);
                Seeds[i].SetActive(false);
                Flowers[i].SetActive(false);
            }
            if (this.tag == "Gold")
            {
                i = 0;
                Terrains[i].SetActive(true);
                Seeds[i].SetActive(true);
            }
            if (this.tag == "Wood")
            {
                i = 1;
                Terrains[i].SetActive(true);
                Seeds[i].SetActive(true);

            }
            if (this.tag == "Water")
            {
                i = 2;
                Terrains[i].SetActive(true);
                Seeds[i].SetActive(true);
            }
            if (this.tag == "Fire")
            {
                i = 3;
                Terrains[i].SetActive(true);
                Seeds[i].SetActive(true);

            }
        }
    }
}
