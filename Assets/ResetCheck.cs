using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetCheck : MonoBehaviour
{
    bool entergame = true;
    public GameObject WholeTerrain;
    public GameObject TriggerRing;
    public ParticleSystem densecloud;
    public ParticleSystem ChangeCloud;
    public GameObject seed;
    public SeedControl control;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider other)
    {

        if(entergame)
        {
            if (other.tag == "Player")
            {
                WholeTerrain.SetActive(false);
                seed.SetActive(false);
                for (int i = 0; i < control.flowers.Length; i++)
                {
                    control.flowers[i].SetActive(false);
                }
                Debug.Log("we back to the first");
                entergame = false;
            }
        }
    }

}
