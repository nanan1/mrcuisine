using LitJson;
using NRKernal;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum DebugState
{
    none,
    up,
    down,
    front,
    back,
    right,
    left,
    big,
    small,
    xadd,
    yadd,
    zadd,
    xreduce,
    yreduce,
    zreduce
}

public class OperateAndDebug : MonoBehaviour
{
    /// <summary>
    /// 实体
    /// </summary>
    public static OperateAndDebug Instance;
    /// <summary>
    /// 高亮对象数组
    /// </summary>
    public HighlightingTest[] hights;
    /// <summary>
    /// 高亮对象数组
    /// </summary>
    public GameObject[] objs;
    /// <summary>
    /// 开启按钮对象
    /// </summary>
    public GameObject openObj;
    /// <summary>
    /// 操作界面对象
    /// </summary>
    public GameObject operationObj;
    /// <summary>
    /// 位置UI对象
    /// </summary>
    public GameObject posObj;
    /// <summary>
    /// 角度UI对象
    /// </summary>
    public GameObject rotObj;
    /// <summary>
    /// 按钮文本显示
    /// </summary>
    public TextMeshProUGUI rotText;
    /// <summary>
    /// 目标控制对象
    /// </summary>
    private GameObject targetObj = null;
    /// <summary>
    /// 移动变量
    /// </summary>
    private float value = 0.001f;
    /// <summary>
    /// 临时对象集合
    /// </summary>
    private List<JsonTemp> tempGoList = new List<JsonTemp>();
    /// <summary>
    /// 当前点击状态
    /// </summary>
    private DebugState debugState = DebugState.none;

    #region Button
    public Button openButton;
    public Button closeButton;
    public Button saveButton;
    public Button rotButton;
    #endregion

    public bool isExists = false;

    void Awake()
    {
        Instance = this;
        Extract();
    }

    void Start()
    {
        //NRVirtualDisplayer(Clone)
        //GameObject vd = GameObject.FindGameObjectWithTag("NRVirtualDisplayer");
        //openObj = vd.transform.Find("VirtualController/Canvas/BaseControllerPanel/OperationUI/Open").gameObject;
        //operationObj = vd.transform.Find("VirtualController/Canvas/BaseControllerPanel/OperationUI/Operation").gameObject;
        //posObj = vd.transform.Find("VirtualController/Canvas/BaseControllerPanel/OperationUI/Operation/Pos").gameObject;
        //rotObj = vd.transform.Find("VirtualController/Canvas/BaseControllerPanel/OperationUI/Operation/Rot").gameObject;
        //rotText = vd.transform.Find("VirtualController/Canvas/BaseControllerPanel/OperationUI/Operation/Pub/Rotation/Text (TMP)").gameObject.GetComponent<TextMeshProUGUI>();
        //openButton = vd.transform.Find("VirtualController/Canvas/BaseControllerPanel/OperationUI/Open").gameObject.GetComponent<Button>();
        //closeButton = vd.transform.Find("VirtualController/Canvas/BaseControllerPanel/OperationUI/Operation/Pub/Close").gameObject.GetComponent<Button>();
        //saveButton = vd.transform.Find("VirtualController/Canvas/BaseControllerPanel/OperationUI/Operation/Pub/Save").gameObject.GetComponent<Button>();
        //rotButton = vd.transform.Find("VirtualController/Canvas/BaseControllerPanel/OperationUI/Operation/Pub/Rotation").gameObject.GetComponent<Button>();


        //openButton.onClick.AddListener(OpenButtonListen);
        //closeButton.onClick.AddListener(CloseButtonListen);
        //saveButton.onClick.AddListener(SaveButtonListen);
        //rotButton.onClick.AddListener(RotButtonListen);

        //读取配置文件给各个对象赋值
        //Extract();
    }

    public void SetDebugState(DebugState state)
    {
        if (debugState != state)
        {
            debugState = state;
        }
    }

    void Update()
    {
        switch (debugState)
        {
            case DebugState.none:
                break;
            case DebugState.up:
                UpButtonListen();
                break;
            case DebugState.down:
                DownButtonListen();
                break;
            case DebugState.front:
                FrontButtonListen();
                break;
            case DebugState.back:
                BackButtonListen();
                break;
            case DebugState.right:
                RightButtonListen();
                break;
            case DebugState.left:
                LeftButtonListen();
                break;
            case DebugState.big:
                BigButtonListen();
                break;
            case DebugState.small:
                SmallButtonListen();
                break;
            case DebugState.xadd:
                RotXAdd();
                break;
            case DebugState.yadd:
                RotYAdd();
                break;
            case DebugState.zadd:
                RotZAdd();
                break;
            case DebugState.xreduce:
                RotXReduce();
                break;
            case DebugState.yreduce:
                RotYReduce();
                break;
            case DebugState.zreduce:
                RotZReduce();
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// 切换控制角色
    /// </summary>
    public void ChangeRole(GameObject go)
    {
        targetObj = go;

        if (hights.Length <= 0)
        {
            return;
        }

        for (int i = 0; i < hights.Length; i++)
        {
            if (hights[i].gameObject.activeInHierarchy)
            {
                if (hights[i].gameObject == targetObj)
                {
                    hights[i].GetNod();
                }
                else
                {
                    hights[i].LostNod();
                }
            }
        }
    }

    /// <summary>
    /// 开启按钮监听方法
    /// </summary>
    private void OpenButtonListen()
    {
        NRInput.SetInputSource(InputSourceEnum.Controller);
        if (hights.Length <= 0)
        {
            return;
        }

        for (int i = 0; i < hights.Length; i++)
        {
            if (hights[i].gameObject.activeInHierarchy)
            {
                hights[i].enabled = true;
                hights[i].OpenHighlighting();
            }
        }

        openObj.SetActive(false);

        if (!operationObj.activeSelf)
        {
            operationObj.SetActive(true);
        }
    }

    /// <summary>
    /// 关闭按钮监听方法
    /// </summary>
    private void CloseButtonListen()
    {
        NRInput.SetInputSource(InputSourceEnum.Hands);
        for (int i = 0; i < hights.Length; i++)
        {
            if (hights[i].gameObject.activeInHierarchy)
            {
                hights[i].CloseHighlighting();
                hights[i].enabled = false;
            }
        }

        operationObj.SetActive(false);

        if (!openObj.activeSelf)
        {
            openObj.SetActive(true);
        }

        targetObj = null;
    }

    private void UpButtonListen()
    {
        if (targetObj == null)
        {
            return;
        }

        if (targetObj.transform.name == "ARBookButton" || targetObj.transform.name == "FilmButton" || targetObj.transform.name == "GameButton" || targetObj.transform.name == "ExitButton" || targetObj.transform.name == "CloseToStart")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y + value * 150f, targetObj.transform.localPosition.z);
        }
        else if (targetObj.transform.name == "UISelect")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y + 1f, targetObj.transform.localPosition.z);
        }
        else
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y + value, targetObj.transform.localPosition.z);
        }
    }

    private void DownButtonListen()
    {
        if (targetObj == null)
        {
            return;
        }

        if (targetObj.transform.name == "ARBookButton" || targetObj.transform.name == "FilmButton" || targetObj.transform.name == "GameButton" || targetObj.transform.name == "ExitButton" || targetObj.transform.name == "CloseToStart")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y - value * 150f, targetObj.transform.localPosition.z);
        }
        else if (targetObj.transform.name == "UISelect")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y - 1f, targetObj.transform.localPosition.z);
        }
        else
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y - value, targetObj.transform.localPosition.z);
        }
    }

    private void FrontButtonListen()
    {
        if (targetObj == null)
        {
            return;
        }

        if (targetObj.transform.name == "ARBookButton" || targetObj.transform.name == "FilmButton" || targetObj.transform.name == "GameButton" || targetObj.transform.name == "ExitButton" || targetObj.transform.name == "CloseToStart")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z + value * 150f);
        }
        else if (targetObj.transform.name == "UISelect")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z + 1f);
        }
        else
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z + value);
        }
    }

    private void BackButtonListen()
    {
        if (targetObj == null)
        {
            return;
        }

        if (targetObj.transform.name == "ARBookButton" || targetObj.transform.name == "FilmButton" || targetObj.transform.name == "GameButton" || targetObj.transform.name == "ExitButton" || targetObj.transform.name == "CloseToStart")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z - value * 150f);
        }
        else if (targetObj.transform.name == "UISelect")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z - 1f);
        }
        else
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z - value);
        }
    }

    private void LeftButtonListen()
    {
        if (targetObj == null)
        {
            return;
        }

        if (targetObj.transform.name == "ARBookButton" || targetObj.transform.name == "FilmButton" || targetObj.transform.name == "GameButton" || targetObj.transform.name == "ExitButton" || targetObj.transform.name == "CloseToStart")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x - value * 150f, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z);
        }
        else if (targetObj.transform.name == "UISelect")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x - 1f, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z);
        }
        else
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x - value, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z);
        }
    }

    private void RightButtonListen()
    {
        if (targetObj == null)
        {
            return;
        }

        if (targetObj.transform.name == "ARBookButton" || targetObj.transform.name == "FilmButton" || targetObj.transform.name == "GameButton" || targetObj.transform.name == "ExitButton" || targetObj.transform.name == "CloseToStart")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x + value * 150f, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z);
        }
        else if (targetObj.transform.name == "UISelect")
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x + 1f, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z);
        }
        else
        {
            targetObj.transform.localPosition = new Vector3(targetObj.transform.localPosition.x + value, targetObj.transform.localPosition.y, targetObj.transform.localPosition.z);
        }
    }

    private void BigButtonListen()
    {
        if (targetObj == null)
        {
            return;
        }

        targetObj.transform.localScale = targetObj.transform.localScale + targetObj.transform.localScale * 0.002f;
    }

    private void SmallButtonListen()
    {
        if (targetObj == null)
        {
            return;
        }

        targetObj.transform.localScale = targetObj.transform.localScale - targetObj.transform.localScale * 0.002f;
    }

    private void SaveButtonListen()
    {
        if (hights.Length <= 0)
        {
            return;
        }

        tempGoList.Clear();
        for (int i = 0; i < hights.Length; i++)
        {
            tempGoList.Add(new JsonTemp());
            tempGoList[i].objName = hights[i].transform.name;
            tempGoList[i].posX = hights[i].transform.localPosition.x.ToString();
            tempGoList[i].posY = hights[i].transform.localPosition.y.ToString();
            tempGoList[i].posZ = hights[i].transform.localPosition.z.ToString();
            tempGoList[i].rotX = hights[i].transform.localEulerAngles.x.ToString();
            tempGoList[i].rotY = hights[i].transform.localEulerAngles.y.ToString();
            tempGoList[i].rotZ = hights[i].transform.localEulerAngles.z.ToString();
            tempGoList[i].sclX = hights[i].transform.localScale.x.ToString();
            tempGoList[i].sclY = hights[i].transform.localScale.y.ToString();
            tempGoList[i].sclZ = hights[i].transform.localScale.z.ToString();
        }

        string json = JsonMapper.ToJson(tempGoList);
        File.WriteAllText(GetPath(), json);
    }

    private void SaveObj()
    {
        if (objs.Length <= 0)
        {
            return;
        }

        tempGoList.Clear();
        for (int i = 0; i < objs.Length; i++)
        {
            tempGoList.Add(new JsonTemp());
            tempGoList[i].objName = objs[i].transform.name;
            tempGoList[i].posX = 0.ToString();
            tempGoList[i].posY = 0.ToString();
            tempGoList[i].posZ = 0.ToString();
            tempGoList[i].rotX = 0.ToString();
            tempGoList[i].rotY = 0.ToString();
            tempGoList[i].rotZ = 0.ToString();
            tempGoList[i].sclX = 0.ToString();
            tempGoList[i].sclY = 0.ToString();
            tempGoList[i].sclZ = 0.ToString();
        }

        string json = JsonMapper.ToJson(tempGoList);
        File.WriteAllText(GetPath(), json);
    }

    private void RotXAdd()
    {
        if (targetObj == null)
        {
            return;
        }

        targetObj.transform.localEulerAngles = new Vector3(targetObj.transform.localEulerAngles.x + value * 100f, targetObj.transform.localEulerAngles.y, targetObj.transform.localEulerAngles.z);
    }

    private void RotYAdd()
    {
        if (targetObj == null)
        {
            return;
        }

        targetObj.transform.localEulerAngles = new Vector3(targetObj.transform.localEulerAngles.x, targetObj.transform.localEulerAngles.y + value * 100f, targetObj.transform.localEulerAngles.z);
    }

    private void RotZAdd()
    {
        if (targetObj == null)
        {
            return;
        }

        targetObj.transform.localEulerAngles = new Vector3(targetObj.transform.localEulerAngles.x, targetObj.transform.localEulerAngles.y, targetObj.transform.localEulerAngles.z + value * 100f);
    }

    private void RotXReduce()
    {
        if (targetObj == null)
        {
            return;
        }

        targetObj.transform.localEulerAngles = new Vector3(targetObj.transform.localEulerAngles.x - value * 100f, targetObj.transform.localEulerAngles.y, targetObj.transform.localEulerAngles.z);
    }

    private void RotYReduce()
    {
        if (targetObj == null)
        {
            return;
        }

        targetObj.transform.localEulerAngles = new Vector3(targetObj.transform.localEulerAngles.x, targetObj.transform.localEulerAngles.y - value * 100f, targetObj.transform.localEulerAngles.z);
    }

    private void RotZReduce()
    {
        if (targetObj == null)
        {
            return;
        }

        targetObj.transform.localEulerAngles = new Vector3(targetObj.transform.localEulerAngles.x, targetObj.transform.localEulerAngles.y, targetObj.transform.localEulerAngles.z - value * 100f);
    }

    private void RotButtonListen()
    {
        if (posObj.activeSelf)
        {
            posObj.SetActive(false);
            rotObj.SetActive(true);
            rotText.text = "Rotation";
        }
        else
        {
            posObj.SetActive(true);
            rotObj.SetActive(false);
            rotText.text = "Position";
        }
    }

    public List<Vector3> posList = new List<Vector3>();
    public List<Vector3> rotList = new List<Vector3>();
    public List<Vector3> scaList = new List<Vector3>();

    private void Extract()
    {
        string path;
#if UNITY_EDITOR
        path = Directory.GetParent(Application.dataPath).FullName + "/" + "Cuisine.json";
#elif UNITY_ANDROID
        path = Application.persistentDataPath + "/" + "Cuisine.json";
#endif

        isExists = File.Exists(path);
        if (isExists)
        {
            List<JsonTemp> tempList = JsonMapper.ToObject<List<JsonTemp>>(File.ReadAllText(path));
            if (tempList.Count > 0)
            {
                for (int i = 0; i < tempList.Count; i++)
                {
                    posList.Add(new Vector3(float.Parse(tempList[i].posX), float.Parse(tempList[i].posY), float.Parse(tempList[i].posZ)));
                    rotList.Add(new Vector3(float.Parse(tempList[i].rotX), float.Parse(tempList[i].rotY), float.Parse(tempList[i].rotZ)));
                    scaList.Add(new Vector3(float.Parse(tempList[i].sclX), float.Parse(tempList[i].sclY), float.Parse(tempList[i].sclZ)));
                }
            }
        }
    }

//    private void Extract()
//    {
//        string path;
//#if UNITY_EDITOR
//        path = Directory.GetParent(Application.dataPath).FullName + "/" + "Cuisine.json";
//#elif UNITY_ANDROID
//        path = Application.persistentDataPath + "/" + "Cuisine.json";
//#endif

//        if (File.Exists(path))
//        {
//            List<JsonTemp> tempList = JsonMapper.ToObject<List<JsonTemp>>(File.ReadAllText(path));
//            if (tempList.Count > 0)
//            {
//                for (int i = 0; i < tempList.Count; i++)
//                {
//                    hights[i].transform.localPosition = new Vector3(float.Parse(tempList[i].posX), float.Parse(tempList[i].posY), float.Parse(tempList[i].posZ));
//                    hights[i].transform.localEulerAngles = new Vector3(float.Parse(tempList[i].rotX), float.Parse(tempList[i].rotY), float.Parse(tempList[i].rotZ));
//                    hights[i].transform.localScale = new Vector3(float.Parse(tempList[i].sclX), float.Parse(tempList[i].sclY), float.Parse(tempList[i].sclZ));
//                }
//            }
//        }
//    }

    public string GetPath()
    {
        string path;
#if UNITY_EDITOR
        path = Directory.GetParent(Application.dataPath).FullName + "/" + "Cuisine.json";
#elif UNITY_ANDROID
        path = Application.persistentDataPath + "/"+ "Cuisine.json";
#endif
        return path;
    }

    public string GetLocalConfig()
    {
        if (File.Exists(GetPath()))
        {
            return File.ReadAllText(GetPath());
        }
        else
        {
            return "";
        }
    }

    private void OnDestroy()
    {
        string path;
#if UNITY_EDITOR
        path = Directory.GetParent(Application.dataPath).FullName + "/" + "Cuisine.json";
#elif UNITY_ANDROID
        path = Application.persistentDataPath + "/" + "Cuisine.json";
#endif

        //if (!File.Exists(path))
        //{
        //    SaveObj();
        //}
    }
}

public class JsonTemp
{
    public string objName;
    public string posX;
    public string posY;
    public string posZ;
    public string rotX;
    public string rotY;
    public string rotZ;
    public string sclX;
    public string sclY;
    public string sclZ;
}
