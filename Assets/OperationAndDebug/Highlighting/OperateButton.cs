using UnityEngine;
using UnityEngine.EventSystems;

public class OperateButton : MonoBehaviour,IPointerDownHandler, IPointerUpHandler
{
    private Vector3 oldScla;

    void Start()
    {
        oldScla = transform.localScale;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        transform.localScale = transform.localScale + transform.localScale * 0.05f;
        switch (transform.name)
        {
            case "Up":
                OperateAndDebug.Instance.SetDebugState(DebugState.up);
                break;
            case "Down":
                OperateAndDebug.Instance.SetDebugState(DebugState.down);
                break;
            case "Front":
                OperateAndDebug.Instance.SetDebugState(DebugState.front);
                break;
            case "Back":
                OperateAndDebug.Instance.SetDebugState(DebugState.back);
                break;
            case "Right":
                OperateAndDebug.Instance.SetDebugState(DebugState.right);
                break;
            case "Left":
                OperateAndDebug.Instance.SetDebugState(DebugState.left);
                break;
            case "Big":
                OperateAndDebug.Instance.SetDebugState(DebugState.big);
                break;
            case "Small":
                OperateAndDebug.Instance.SetDebugState(DebugState.small);
                break;
            case "X+":
                OperateAndDebug.Instance.SetDebugState(DebugState.xadd);
                break;
            case "Y+":
                OperateAndDebug.Instance.SetDebugState(DebugState.yadd);
                break;
            case "Z+":
                OperateAndDebug.Instance.SetDebugState(DebugState.zadd);
                break;
            case "X-":
                OperateAndDebug.Instance.SetDebugState(DebugState.xreduce);
                break;
            case "Y-":
                OperateAndDebug.Instance.SetDebugState(DebugState.yreduce);
                break;
            case "Z-":
                OperateAndDebug.Instance.SetDebugState(DebugState.zreduce);
                break;
            default:
                break;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        transform.localScale = oldScla;
        OperateAndDebug.Instance.SetDebugState(DebugState.none);
    }
}
