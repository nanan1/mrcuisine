using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BoxCollider))]
public class HighlightingTest : FlashingController,IPointerDownHandler,IPointerUpHandler
{
    private BoxCollider boxCollider;

    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    /// <summary>
    /// 开启高亮
    /// </summary>
    public void OpenHighlighting()
    {
        if (boxCollider == null)
        {
            boxCollider = GetComponent<BoxCollider>();
        }

        boxCollider.enabled = true;
        LostNod();
    }

    /// <summary>
    /// 被选中
    /// </summary>
    public void GetNod()
    {
        ho.FlashingOn(flashingStartColor);
    }

    /// <summary>
    /// 丢失选中
    /// </summary>
    public void LostNod()
    {
        ho.FlashingOn(flashingEndColor);
    }

    /// <summary>
    /// 关闭高亮
    /// </summary>
    public void CloseHighlighting()
    {
        if (boxCollider == null)
        {
            return;
        }

        boxCollider.enabled = false;
        ho.FlashingOff();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OperateAndDebug.Instance.ChangeRole(gameObject);
    }
}
