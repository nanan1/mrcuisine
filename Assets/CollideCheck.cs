using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideCheck : MonoBehaviour
{
    bool entergame = false;
    public GameObject WholeTerrain;
    public GameObject TriggerRing;
    public ParticleSystem densecloud;
    public ParticleSystem ChangeCloud;
    public GameObject seed;
    public ParticleSystem[] booming;
    public SeedControl control;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider other)
    {
        if(!entergame)
        {
            if (other.tag == "Player" && this.tag == "Gaming")
            {
                TriggerRing.SetActive(false);
                ChangeCloud.Play();
                Invoke("hidecloud", 0.5f);
                Debug.Log("we enter the trig");
                for (int i = 0; i < booming.Length; i++)
                {
                    Vector3 seedposition = control.seeds[i].transform.position;
                    booming[i].transform.position = new Vector3(seedposition.x, seedposition.y, seedposition.z);
                    booming[i].Play();
                }
                entergame = true;
            }
            else
            {
                return;
            }
        }

        if(entergame)
        {
            Debug.Log("enter game is true");
            if (other.tag == "Player" && this.tag == "Reset")
            {
                WholeTerrain.SetActive(false);
                seed.SetActive(false);
                for (int i = 0; i < control.flowers.Length; i++)
                {
                    control.flowers[i].SetActive(false);
                }
                TriggerRing.SetActive(false);
                Debug.Log("we back to the first");
                entergame = false;
            }
        }
    }

    public void hidecloud()
    {
        densecloud.Stop();
        WholeTerrain.SetActive(true);
        seed.SetActive(true);
        Debug.Log("we hide the densecloud");
    }

}
