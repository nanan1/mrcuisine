using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagCheck : MonoBehaviour
{
    public SeedControl control;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void check()
    {
        if(this.tag == "Gold")
        {
            control.num = 0;
        }
        else if (this.tag == "Wood")
        {
            control.num = 1;
        }
        else if (this.tag == "Water")
        {
            control.num = 2;
        }
        else if (this.tag == "Fire")
        {
            control.num = 3;
        }
        else if (this.tag == "Gold2")
        {
            control.num = 4;
        }
        else if (this.tag == "Wood2")
        {
            control.num = 5;
        }
        else if (this.tag == "Water2")
        {
            control.num = 6;
        }
        else if (this.tag == "Fire2")
        {
            control.num = 7;
        }
        control.delayseed();
    }

}
