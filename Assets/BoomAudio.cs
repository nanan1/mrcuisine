using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomAudio : MonoBehaviour
{
    public AudioClip peng;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void boomaudio()
    {
        audioSource.PlayOneShot(peng, 1F);
    }

    public void boomdelayed()
    {
        Invoke("boomaudio", 5f);
    }
}
