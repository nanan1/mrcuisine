using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedControl : MonoBehaviour
{
    public int num;
    private int hack;
    public GameObject[] seeds;
    public GameObject[] flowers;
    public GameObject[] terrains;
    public ParticleSystem[] boom;
    bool hastouch = false;
    float timer = 0;
    bool starttimer= false;
    public ParticleSystem changecloud;
    public GameObject WholeTerrain;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(starttimer)
        {
            timer += Time.deltaTime;
            Debug.Log("��ʱ����" + timer);
            if(timer>=15)
            {
                starttimer = false;
                timer = 0f;
                Invoke("hideflower", 0f);
            }
        }
    }
    public void delayseed()
    {
        if (!hastouch)
        {
            if (num > 3)
            {
                hack = -1;
            }
            else
            {
                hack = 1;
            }
            Vector3 seedinitial = seeds[num].transform.position;
            Invoke("seed", 3.5f);
            Invoke("booming", 3f);
            hastouch = true;
        }
        else
        {
            return;
        }
    }

    public void booming()
    {
        Vector3 seedfinal = seeds[num].transform.position;
        boom[num].transform.position = new Vector3 (seedfinal.x, seedfinal.y, seedfinal.z); // - 0.8f *hack
        boom[num].Play();
        this.GetComponent<BoomAudio>().boomaudio();
        Debug.Log("Boom Complete");
        changecloud.Play();

    }
    public void seed()
    {
        for (int num = 0; num < flowers.Length; num++)
        {
            flowers[num].SetActive(false);
            terrains[num].SetActive(false);
        }
        WholeTerrain.SetActive(false);
        seeds[num].SetActive(false);
        Vector3 seedfinal = seeds[num].transform.position;
        flowers[num].transform.position = new Vector3(seedfinal.x, seedfinal.y, seedfinal.z);
        flowers[num].SetActive(true);
        Invoke("changeterrain", 0f);
        Debug.Log("seed has been canceled");
        hastouch = false;
        timer = 0;
        starttimer = true;

    }

    public void hideflower()
    {
        Debug.Log("we inter timer");
        for (int num = 0; num < flowers.Length; num++)
        {
            seeds[num].SetActive(true);

        }
        terrains[num].SetActive(false);
        flowers[num].SetActive(false);
        WholeTerrain.SetActive(true);
        //seeds[num].transform.position = new Vector3(seedinitial.x, seedfinal.y, seedfinal.z - 0.8f * hack);
    }

    public void changeterrain()
    {
        terrains[num].SetActive(true);
        Debug.Log("we change terrain");
    }
}
