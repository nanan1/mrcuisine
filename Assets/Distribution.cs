using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Immersal.AR.Nreal;

public class Distribution : MonoBehaviour
{
    public static Distribution Instance;
    private bool first = false;
    private float timer = 0 ;
    private float initialtime = 5f;
    public NRLocalizer nrLoccalizer;
    public ImageTracking imageTracking;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        first = false;
        timer = 0f;
        nrLoccalizer.localizationInterval = 0.2f;
        nrLoccalizer.autoStart = true;
        nrLoccalizer.StartLocalizing();
    }

    /// <summary>
    /// ���õ���ʱ������
    /// </summary>
    public void SetFirst()
    {
        first = true;
    }

    void Update()
    {
        if(first)
        {
            timer += Time.deltaTime;
            if(timer >= initialtime)
            {
                nrLoccalizer.localizationInterval = 2f;
                nrLoccalizer.autoStart = false;
                nrLoccalizer.StopLocalizing();
                imageTracking.EnableImageTracking();
                first = false;
            }
        }
    }

}
