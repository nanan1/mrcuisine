using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NRKernal;
/// <summary>
/// ��ҿ�����
/// </summary>
public class PlayerController : MonoBehaviour
{
    [HideInInspector] private Transform mTrans;
    public Transform trans
    {
        get { return mTrans; }
    }
    [HideInInspector] private CharacterController mController;
    public CharacterController controller
    {
        get { return mController; }
    }
    // Start is called before the first frame update
    void Start()
    {
        mTrans = transform;
        mController = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (NRInput.CameraCenter == null) return;
        //    //hit route point
        Vector3 to = NRInput.CameraCenter.position;//Camera.main.transform.position;
        mTrans.position = to; //new Vector3(-mParent.localEulerAngles.x, 0, -mParent.localEulerAngles.z);//mParent.eulerAngles *Vector3.zero;
        Vector3 angle = Vector3.zero;
        angle.y = NRInput.CameraCenter.eulerAngles.y;
        mTrans.eulerAngles = angle; //new Vector3(-mParent.eulerAngles.x, mParent.eulerAngles.y, -mParent.eulerAngles.z);//mParent.eulerAngles *Vector3.zero;
    }

    public void Clear()
    {
        DestroyImmediate(this, true);
    }
}
